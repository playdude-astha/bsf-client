/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SET_USER = 'awesome-fe/App/SET_USER';
export const GET_USER = 'awesome-fe/App/GET_USER';
export const LOGIN = 'awesome-fe/App/LOGIN';
export const GET_LOGOUT = 'awesome-fe/App/GET_LOGOUT';
export const SET_LOGOUT = 'awesome-fe/App/SET_LOGOUT';
