/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import Dashboard from 'containers/Dashboard/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import saga from './saga';
import reducer from './reducer';
import GlobalStyle from '../../global-styles';

import './style.scss';
import { getUser } from './actions';
import { makeSelectUser } from './selectors';
const key = 'app';

export function App(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const { user, getUserDetails } = props;

  useEffect(() => {
    console.log(user);
    if (user.status === null) {
      getUserDetails();
    }
  }, [user.status]);

  console.log(user);

  return (
    <div className="app-container">
      <Switch>
        <Route exact path="/" component={HomePage} />
        {user.status === 1 ? (
          <Route path="/dashboard" component={Dashboard} />
        ) : null}
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </div>
  );
}

App.propTypes = {
  user: PropTypes.object,
  getUserDetails: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getUserDetails: () => dispatch(getUser()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(App);
