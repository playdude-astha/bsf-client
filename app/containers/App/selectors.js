/**
 * The global state selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectApp = state => state.app || initialState;

const makeSelectUser = () =>
  createSelector(
    selectApp,
    appState => appState.user,
  );

export { selectApp as selectGlobal, makeSelectUser };
