import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import Axios from 'axios';

// import request from 'utils/request';
import { GET_USER, LOGIN, GET_LOGOUT } from './constants';
import { setUser, setLogout } from './actions';

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(GET_USER, getUserDetails);
  yield takeLatest(LOGIN, login);
  yield takeLatest(GET_LOGOUT, logout);
}

export const getUser = async () =>
  Axios.get(`api/user/login`).then(res => {
    if (res.status === 200) {
      return res.data;
    }
    throw new Error('Not Logged in');
  });

export const loginUser = async body =>
  Axios.post(`api/user/login`, body).then(res => {
    if (res.status === 200) {
      return res.data;
    }
    throw new Error('Not Logged in');
  });

export const loginOutUser = async () =>
  Axios.get(`api/user/logout`).then(res => {
    if (res.status === 200) {
      return res.data;
    }
    throw new Error('Not Logged in');
  });

/**
 * Github repos request/response handler
 */
export function* getUserDetails() {
  // const requestURL = `/api/user/login`;

  try {
    // Call our request helper (see 'utils/request')
    const user = yield call(getUser);

    if (user && user.user) {
      yield put(
        setUser({
          status: 1,
          data: user.user,
        }),
      );
      yield put(push('/dashboard'));
    } else {
      throw new Error(500);
    }
  } catch (err) {
    yield put(
      setUser({
        status: -1,
        data: null,
      }),
      // setUser({
      //   status: 1,
      //   data: {
      //     settings: {
      //       news: 'Welcome to Exchange',
      //       stakeButtons: [],
      //       betLock: false,
      //       accountLock: false,
      //       betLockedBy: '',
      //       accountLockedBy: '',
      //       betTime: 0,
      //     },
      //     account: {
      //       settled: 0,
      //       sharing: 100,
      //       commission: 0,
      //       points: 10000000,
      //       coinValue: 1,
      //       exposure: 0,
      //       downlinkPoints: 0,
      //     },
      //     tokens: [],
      //     sessions: [],
      //     agents: [],
      //     level: 1,
      //     _id: '5f91a315900301e96d77c069',
      //     username: 'admin@bsf',
      //     name: 'Super Admin',
      //     email: 'superadmin@bsf.com',
      //     role: 'SUPER_ADMIN',
      //     createdAt: '2020-10-22T15:19:49.399Z',
      //     updatedAt: '2020-10-22T15:19:49.399Z',
      //     __v: 0,
      //   },
      // }),
    );
    yield put(push('/'));
  }
}

export function* login(cred) {
  // const requestURL = `https://api.skr365.com/api/user/login`;

  try {
    // Call our request helper (see 'utils/request')
    const user = yield call(loginUser, cred.payload);

    if (user && user.user) {
      yield put(
        setUser({
          status: 1,
          data: user.user,
        }),
      );
      yield put(push('/dashboard'));
    } else {
      throw new Error(500);
    }
  } catch (err) {
    yield put(
      setUser({
        status: -1,
        data: null,
      }),
    );
  }
}

export function* logout() {
  // const requestURL = `https://api.skr365.com/api/user/logout`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(loginOutUser);

    if (response && response.message === 'logged out') {
      yield put(
        setLogout({
          status: null,
          data: null,
        }),
      );
      yield put(push('/'));
    } else {
      throw new Error(500);
    }
  } catch (err) {
    yield put(
      setLogout({
        status: -1,
        data: null,
      }),
    );
  }
}
