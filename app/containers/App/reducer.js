/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { SET_USER, GET_USER, LOGIN, GET_LOGOUT, SET_LOGOUT } from './constants';

// The initial state of the App
export const initialState = {
  user: {
    status: null,
    data: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_USER:
        draft.user = {
          status: action.payload.status,
          data: action.payload.data,
        };
        break;
      case GET_USER:
        draft.user = {
          status: 0,
          data: null,
        };
        break;
      case LOGIN:
        draft.user = {
          status: 0,
          data: null,
        };
        break;
      case GET_LOGOUT:
        // draft.user = {
        //   status: null,
        //   data: null,
        // };
        break;
      case SET_LOGOUT:
        draft.user = {
          status: action.payload.status,
          data: action.payload.data,
        };
        break;
    }
  });

export default appReducer;
