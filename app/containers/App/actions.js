/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { SET_USER, GET_USER, LOGIN, GET_LOGOUT, SET_LOGOUT } from './constants';

export function setUser(payload) {
  return {
    type: SET_USER,
    payload,
  };
}

export function getUser(payload) {
  return {
    type: GET_USER,
    payload,
  };
}

export function login(payload) {
  return {
    type: LOGIN,
    payload,
  };
}

export function getLogout(payload) {
  return {
    type: GET_LOGOUT,
    payload,
  };
}

export function setLogout(payload) {
  return {
    type: SET_LOGOUT,
    payload,
  };
}
