/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectHomeContainer = () =>
  createSelector(
    selectHome,
    homeState => homeState,
  );
// const makeSelectUser = () =>
//   createSelector(
//     selectHome,
//     homeState => homeState.user,
//   );

export default makeSelectHomeContainer;
// export { makeSelectUser };
