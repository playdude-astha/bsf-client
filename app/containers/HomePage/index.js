/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useState, memo, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Container,
  InputGroup,
  FormControl,
  Button,
  Spinner,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { Helmet } from 'react-helmet';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';

import './style.scss';
import { makeSelectUser } from '../App/selectors';
import { login } from '../App/actions';
const key = 'home';

export function HomePage(props) {
  const { user, loginUser } = props;
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const triggerLogin = () => {
    if (!username || !password) return;
    loginUser({ username, password });
  };

  const enterPressed = event => {
    const code = event.keyCode || event.which;
    if (code === 13) triggerLogin();
  };

  useEffect(() => {
    // login failed case
    if (user.status === -1) {
      setUsername('');
      setPassword('');
    }
    if (user.status === 1) {
      props.history.push('/dashboard');
    }
  }, [user.status]);

  console.log(user);

  return (
    <Container fluid className="home-container">
      <Helmet>
        <title>Home</title>
        <meta name="Home" content="Home" />
      </Helmet>
      <div className="login-box">
        <h1>
          <FormattedMessage {...messages.logo} />
        </h1>
        <h3>
          <FormattedMessage {...messages.title1} />
        </h3>
        <h3>
          <FormattedMessage {...messages.title2} />
        </h3>
        <div className="form-container">
          <InputGroup className="mb-6">
            <FormControl
              placeholder="Username"
              aria-label="Username"
              onChange={ev => setUsername(ev.target.value)}
              value={username}
              disabled={user.status === 0}
            />
          </InputGroup>
          <InputGroup className="mb-6">
            <FormControl
              placeholder="Password"
              type="password"
              aria-label="Password"
              onChange={ev => setPassword(ev.target.value)}
              value={password}
              onKeyPress={enterPressed}
              disabled={user.status === 0}
            />
          </InputGroup>
          <Button
            className="login-btn"
            variant="info"
            onClick={triggerLogin}
            disabled={!username || !password || user.status === 0}
          >
            {user.status === 0 ? (
              <>
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                <span className="sr-only">Loading...</span>
              </>
            ) : (
              <FormattedMessage {...messages.loginButton} />
            )}
          </Button>
          <p>
            <FormattedMessage {...messages.copyright} />
          </p>
        </div>
      </div>
    </Container>
  );
}
HomePage.propTypes = {
  user: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loginUser: payload => dispatch(login(payload)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
