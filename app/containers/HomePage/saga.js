// import { call, put, takeLatest } from 'redux-saga/effects';

// import request from 'utils/request';
// import { push } from 'connected-react-router';
// // import { LOGIN } from './constants';
// import { setUser } from '../App/actions';

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield takeLatest(LOGIN, login);
}

/**
 * Github repos request/response handler
 */
// export function* login(cred) {
//   const requestURL = `http://api.skr365.com/api/user/login`;

//   try {
//     // Call our request helper (see 'utils/request')
//     const user = yield call(request, requestURL, {
//       method: 'POST',
//       headers: {
//         'Content-type': 'application/json',
//       },
//       body: JSON.stringify(cred.payload),
//     });

//     if (user && user.user) {
//       yield put(
//         setUser({
//           status: 1,
//           data: user.user,
//         }),
//       );
//       yield put(push('/dashboard'));
//     } else {
//       throw new Error(500);
//     }
//   } catch (err) {
//     yield put(
//       setUser({
//         status: -1,
//         data: null,
//       }),
//     );
//   }
// }
