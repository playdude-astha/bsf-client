/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  logo: {
    id: `${scope}.logo`,
    defaultMessage: 'SKR365',
  },
  title1: {
    id: `${scope}.title1`,
    defaultMessage: 'Welcome to SKR365',
  },
  title2: {
    id: `${scope}.title2`,
    defaultMessage: 'Sign In',
  },
  loginButton: {
    id: `${scope}.loginButton`,
    defaultMessage: 'Login',
  },
  copyright: {
    id: `${scope}.copyright`,
    defaultMessage: 'SKR365 © 2020',
  },
});
