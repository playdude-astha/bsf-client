/**
 *
 * Asynchronously loads the component for Stockists
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
