/*
 *
 * Stockists reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  CREATE_STOCKIST,
  STOCKIST_CREATED,
  FETCH_STOCKISTS,
  STOCKISTS_FETCHED,
} from './constants';

export const initialState = {
  newStockist: {
    status: null,
    data: null,
  },
  stockistList: {
    status: null,
    data: null,
  },
};

/* eslint-disable default-case, no-param-reassign */
const stockistsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case CREATE_STOCKIST:
        draft.newStockist = {
          status: 0,
          data: null,
        };
        break;
      case STOCKIST_CREATED:
        draft.newStockist = {
          status: action.payload.status,
          data: action.payload.data,
        };
        break;
      case FETCH_STOCKISTS:
        draft.stockistList = {
          status: 0,
          data: null,
        };
        break;
      case STOCKISTS_FETCHED:
        draft.stockistList = {
          status: action.payload.status,
          data: action.payload.data,
        };
        break;
    }
  });

export default stockistsReducer;
