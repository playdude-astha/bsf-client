/*
 *
 * Stockists constants
 *
 */

export const DEFAULT_ACTION = 'app/Stockists/DEFAULT_ACTION';
export const CREATE_STOCKIST = 'app/Stockists/CREATE_STOCKIST';
export const STOCKIST_CREATED = 'app/Stockists/STOCKIST_CREATED';
export const FETCH_STOCKISTS = 'app/Stockists/FETCH_STOCKISTS';
export const STOCKISTS_FETCHED = 'app/Stockists/STOCKISTS_FETCHED';
