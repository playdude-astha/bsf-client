import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the stockists state domain
 */

const selectStockistsDomain = state => state.stockists || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Stockists
 */

const makeSelectStockists = () =>
  createSelector(
    selectStockistsDomain,
    substate => substate,
  );

const makeSelectNewStockists = () =>
  createSelector(
    selectStockistsDomain,
    substate => substate.newStockist,
  );

const makeSelectStockistsList = () =>
  createSelector(
    selectStockistsDomain,
    substate => substate.stockistList,
  );

export default makeSelectStockists;
export {
  selectStockistsDomain,
  makeSelectNewStockists,
  makeSelectStockistsList,
};
