// import { take, call, put, select } from 'redux-saga/effects';

import { call, put, takeLatest } from 'redux-saga/effects';
// import request from 'utils/request';
import Axios from 'axios';
import { CREATE_STOCKIST, FETCH_STOCKISTS } from './constants';
import { stockistCreated, stockistsFetched } from './actions';

// Individual exports for testing
export default function* stockistsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(CREATE_STOCKIST, createStockist);
  yield takeLatest(FETCH_STOCKISTS, fetchStockists);
}

export const create = async body =>
  Axios.post(`api/user`, body).then(res => {
    if (res.status === 200) {
      return res.data;
    }
    throw new Error('Not Logged in');
  });

export const getList = async config =>
  Axios.get(`api/user/list?level=${config.level}`).then(res => {
    if (res.status === 200) {
      return res.data;
    }
    throw new Error('Not Logged in');
  });

export function* createStockist(data) {
  // const requestURL = `https://api.skr365.com/api/user`;

  try {
    // Call our request helper (see 'utils/request')
    const stockist = yield call(create, data.payload);

    if (stockist) {
      yield put(
        stockistCreated({
          status: 1,
          data: stockist,
        }),
      );
    } else {
      throw new Error(500);
    }
  } catch (err) {
    yield put(
      stockistCreated({
        status: -1,
        data: null,
      }),
    );
  }
}

export function* fetchStockists(config) {
  // const requestURL = `https://api.skr365.com/api/user/list`;

  try {
    // Call our request helper (see 'utils/request')
    const stockists = yield call(getList, config.payload);

    if (stockists && stockists.list) {
      yield put(
        stockistsFetched({
          status: 1,
          data: stockists.list || [],
        }),
      );
    } else {
      throw new Error(500);
    }
  } catch (err) {
    yield put(
      stockistsFetched({
        status: -1,
        data: null,
      }),
    );
  }
}
