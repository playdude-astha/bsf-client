/**
 *
 * Stockists
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import StockistsForm from 'components/StockistsForm';
import { Button, Alert, Table } from 'react-bootstrap';
import makeSelectStockists, {
  makeSelectNewStockists,
  makeSelectStockistsList,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import { createStockist, fetchStockists } from './actions';

import './style.scss';

function StockistRow(props) {
  const { username, name, account } = props;
  return (
    <tr key={username}>
      <td>{username}</td>
      <td>{name}</td>
      <td>{account && account.sharing}</td>
      <td>{account && account.sharing}</td>
      <td>
        <button
          type="button"
          onClick={() => {
            console.log('edit user', username);
          }}
        >
          Edit
        </button>
        <button
          type="button"
          onClick={() => {
            console.log('change password: ', username);
          }}
        >
          Change Password
        </button>
      </td>
    </tr>
  );
}

StockistRow.propTypes = {
  username: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  // role: PropTypes.string.isRequired,
  account: PropTypes.object.isRequired,
};

export function Stockists(props) {
  useInjectReducer({ key: 'stockists', reducer });
  useInjectSaga({ key: 'stockists', saga });

  const {
    newStockist,
    createStockistFun,
    getStockists,
    stockistList,
    config,
    title,
  } = props;

  console.log('config: ', config.level, config.role);
  const [showForm, setShowForm] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    console.log(newStockist);
    if (newStockist.status === 1) {
      setShowForm(false);
    }
    if (newStockist.status === -1) {
      setShowAlert(true);
    }
  }, [newStockist.status]);

  useEffect(() => {
    if (newStockist.status === 1 || newStockist.status === null) {
      getStockists(config);
    }
  }, [newStockist.status, config.level]);

  const createUser = data => {
    createStockistFun(data);
  };

  if (showAlert) {
    return (
      <Alert variant="danger" onClose={() => setShowAlert(false)} dismissible>
        <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
        <p>Stockist creation failed somehow, please try creating again!</p>
      </Alert>
    );
  }

  return (
    <div className="stockists-wrapper">
      <Helmet>
        <title>{title}</title>
        <meta name="description" content="Description of Stockists" />
      </Helmet>
      {/* <FormattedMessage {...messages.header} /> */}
      {showForm ? (
        <>
          <div className="tool-bar">
            <Button variant="danger" onClick={() => setShowForm(false)}>
              Cancel
            </Button>
          </div>
          <StockistsForm
            createStockist={createUser}
            status={newStockist.status}
            userRole={config.role}
            level={config.level}
          />
        </>
      ) : (
        <>
          <div className="tool-bar">
            <Button variant="info" onClick={() => setShowForm(true)}>
              Create
            </Button>
            <h3>All {title}</h3>
          </div>
          <div className="table-container">
            <Table striped bordered hover variant="dark" size="sm">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Match Commission</th>
                  <th>Session Commission</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {stockistList.status === 0 ? `Fetching ${title}` : null}
                {stockistList.status === -1
                  ? `Failed to fetch ${title}!`
                  : null}
                {stockistList.status === 1 && stockistList.data.length === 0
                  ? `No ${title} Found!`
                  : null}
                {/* eslint-disable */}
                {stockistList.status === 1 && stockistList.data.length > 0
                  ? stockistList.data.map(stockist => (
                      <StockistRow {...stockist} />
                    ))
                  : null}
              </tbody>
            </Table>
          </div>
        </>
      )}
    </div>
  );
}

Stockists.propTypes = {
  newStockist: PropTypes.object.isRequired,
  createStockistFun: PropTypes.func.isRequired,
  getStockists: PropTypes.func.isRequired,
  stockistList: PropTypes.object.isRequired,
  config: PropTypes.object,
  title: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  stockists: makeSelectStockists(),
  newStockist: makeSelectNewStockists(),
  stockistList: makeSelectStockistsList(),
});

function mapDispatchToProps(dispatch) {
  return {
    createStockistFun: data => dispatch(createStockist(data)),
    getStockists: config => dispatch(fetchStockists(config)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Stockists);
