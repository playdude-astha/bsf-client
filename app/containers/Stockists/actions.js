/*
 *
 * Stockists actions
 *
 */

import {
  DEFAULT_ACTION,
  CREATE_STOCKIST,
  STOCKIST_CREATED,
  FETCH_STOCKISTS,
  STOCKISTS_FETCHED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function createStockist(payload) {
  return {
    type: CREATE_STOCKIST,
    payload,
  };
}

export function stockistCreated(payload) {
  return {
    type: STOCKIST_CREATED,
    payload,
  };
}

export function fetchStockists(payload) {
  return {
    type: FETCH_STOCKISTS,
    payload,
  };
}

export function stockistsFetched(payload) {
  return {
    type: STOCKISTS_FETCHED,
    payload,
  };
}
