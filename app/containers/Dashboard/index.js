/**
 *
 * Dashboard
 *
 */

import React, { useState } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { FaGem, FaBars, FaPeopleCarry } from 'react-icons/fa';
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarContent,
  SidebarFooter,
} from 'react-pro-sidebar';
import Stockists from 'containers/Stockists/Loadable';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Container, Navbar, Button } from 'react-bootstrap';
import { withWindow } from 'components/Hoc/resize';
import makeSelectDashboard from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import './style.scss';
import { getLogout } from '../App/actions';
import { getManageRoutes } from '../../config';
import { makeSelectUser } from '../App/selectors';

export function Dashboard(props) {
  useInjectReducer({ key: 'dashboard', reducer });
  useInjectSaga({ key: 'dashboard', saga });

  const { isTablet, match, logout, user } = props;

  const [collapsed, setCollapsed] = useState(false);
  const [toggled, setToggled] = useState(false);

  const handleCollapsedChange = value => {
    setCollapsed(value);
    return true;
  };

  const handleToggleSidebar = value => {
    setToggled(value);
  };

  const routes = getManageRoutes(user.data.level, user.data.role);

  return (
    <div className="dashboard-container">
      <Helmet>
        <title>Dashboard</title>
        <meta name="description" content="Description of Dashboard" />
      </Helmet>
      <ProSidebar
        collapsed={collapsed}
        toggled={toggled}
        breakPoint="md"
        onToggle={handleToggleSidebar}
      >
        <SidebarHeader className="side-heading">
          <h2>
            <FormattedMessage {...messages.logo} />
          </h2>
        </SidebarHeader>
        <SidebarContent>
          <Menu iconShape="square">
            <MenuItem icon={<FaGem />}>
              <Link to={`${match.path}`}>Dashboard</Link>
            </MenuItem>
            {routes.length > 0 ? (
              <SubMenu title="Manage" icon={<FaPeopleCarry />}>
                {routes.map(r => (
                  <MenuItem>
                    <Link to={`${match.path}/${r.path}`}>{r.menuTitle}</Link>
                  </MenuItem>
                ))}
              </SubMenu>
            ) : (
              <></>
            )}
          </Menu>
        </SidebarContent>
        <SidebarFooter className="side-footer">
          <p>
            <FormattedMessage {...messages.copyright} />
          </p>
        </SidebarFooter>
      </ProSidebar>
      <Container className="main-content">
        <Navbar className="main-navbar">
          <FaBars
            onClick={() =>
              isTablet
                ? handleCollapsedChange(false) && handleToggleSidebar(!toggled)
                : handleCollapsedChange(!collapsed)
            }
            className="toggle-btn"
          />
          <Navbar.Brand>Dashboard</Navbar.Brand>
          <Button
            variant="light"
            className="logout-btn"
            onClick={() => logout()}
          >
            Logout
          </Button>
        </Navbar>
        {routes.length > 0 ? (
          <Switch>
            {routes.map(r => (
              <Route
                exact
                path={`${match.path}/${r.path}`}
                render={p => (
                  <Stockists {...p} config={r.config} title={r.pageTitle} />
                )}
              />
            ))}
          </Switch>
        ) : null}
      </Container>
    </div>
  );
}

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isTablet: PropTypes.bool,
  match: PropTypes.object,
  logout: PropTypes.func,
  user: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  dashboard: makeSelectDashboard(),
  user: makeSelectUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(getLogout()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(withWindow(Dashboard));
