/*
 * Dashboard Messages
 *
 * This contains all the text for the Dashboard container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Dashboard';

export default defineMessages({
  logo: {
    id: `${scope}.logo`,
    defaultMessage: 'SKR365',
  },
  copyright: {
    id: `${scope}.copyright`,
    defaultMessage: 'SKR365 © 2020',
  },
});
