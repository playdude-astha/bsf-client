/**
 *
 * StockistsForm
 *
 */

import React, { useState } from 'react';
import {
  Form,
  Row,
  Col,
  Button,
  Modal,
  InputGroup,
  FormControl,
} from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

const schema = Yup.object({
  username: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
  confirmPassword: Yup.string().required('Required'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Required'),
  name: Yup.string().required('Required'),
});

function ConfirmModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Confirm Creation
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          <InputGroup className="mb-3">
            <FormControl
              placeholder="Enter Your Password"
              type="password"
              aria-label="pass"
              aria-describedby="pass"
              value={props.myPass}
              onChange={ev => props.setMyPass(ev.target.value)}
              required
            />
          </InputGroup>
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button
          disabled={!props.myPass}
          onClick={() => {
            if (props.myPass) {
              props.createStockist({
                ...props.formData,
                myPassword: props.myPass,
                confirmPassword: undefined,
              });
            }
            props.onHide();
          }}
        >
          Confim
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

ConfirmModal.propTypes = {
  setMyPass: PropTypes.func.isRequired,
  createStockist: PropTypes.func.isRequired,
  formData: PropTypes.object.isRequired,
  myPass: PropTypes.string.isRequired,
  onHide: PropTypes.func.isRequired,
};

function StockistsForm(props) {
  const { createStockist, status, userRole, level } = props;
  const initialFormData = {
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    name: '',
    level,
    commission: 0,
    sharing: 0,
    userRole,
  };
  const [formData, setFormData] = useState(initialFormData);
  const [modalShow, setModalShow] = useState(false);
  const [myPass, setMyPass] = useState('');
  return (
    <>
      <Formik
        validationSchema={schema}
        validate={values => {
          const errors = {};
          if (values.confirmPassword !== values.password) {
            errors.confirmPassword = 'Password Mismatch';
          }
          return errors;
        }}
        initialValues={{ ...formData }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            // alert(JSON.stringify(values, null, 2));
            setFormData(values);
            setModalShow(true);
            // createStockist(data);
            // resetForm({ ...initialFormData });
            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <Form onSubmit={handleSubmit}>
            <Form.Group as={Row} controlId="name">
              <Form.Label column sm={2}>
                Name
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="text"
                  placeholder="Name"
                  value={values.name}
                  onChange={handleChange}
                  isInvalid={touched.name && !!errors.name}
                  disabled={isSubmitting}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.name}
                </Form.Control.Feedback>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="username">
              <Form.Label column sm={2}>
                User Name
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="text"
                  placeholder="User Name"
                  value={values.username}
                  onChange={handleChange}
                  isInvalid={touched.username && !!errors.username}
                  disabled={isSubmitting}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.username}
                </Form.Control.Feedback>
              </Col>
            </Form.Group>

            {/* <Form.Group as={Row} controlId="firstName">
          <Form.Label column sm={2}>
            First Name
          </Form.Label>
          <Col sm={10}>
            <Form.Control type="text" placeholder="First Name" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="lastName">
          <Form.Label column sm={2}>
            Last Name
          </Form.Label>
          <Col sm={10}>
            <Form.Control type="text" placeholder="Last Name" />
          </Col>
        </Form.Group> */}

            <Form.Group as={Row} controlId="email">
              <Form.Label column sm={2}>
                Email
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="text"
                  placeholder="Email"
                  value={values.email}
                  onChange={handleChange}
                  isInvalid={touched.email && !!errors.email}
                  disabled={isSubmitting}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </Col>
            </Form.Group>

            {/* <Form.Group as={Row} controlId="level">
              <Form.Label column sm={2}>
                Level
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="number"
                  placeholder="level"
                  step="1"
                  value={values.level}
                  onChange={handleChange}
                  disabled={isSubmitting}
                />
              </Col>
            </Form.Group> */}

            <Form.Group as={Row} controlId="commission">
              <Form.Label column sm={2}>
                Commission
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="number"
                  placeholder="Commission"
                  step="0.01"
                  value={values.commission}
                  onChange={handleChange}
                  disabled={isSubmitting}
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="sharing">
              <Form.Label column sm={2}>
                Sharing
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="number"
                  placeholder="Sharing"
                  step="0.01"
                  value={values.sharing}
                  onChange={handleChange}
                  disabled={isSubmitting}
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="password">
              <Form.Label column sm={2}>
                Password
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={values.password}
                  onChange={handleChange}
                  isInvalid={touched.password && !!errors.password}
                  disabled={isSubmitting}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.password}
                </Form.Control.Feedback>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="confirmPassword">
              <Form.Label column sm={2}>
                Confirm Password
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  isInvalid={
                    touched.confirmPassword && !!errors.confirmPassword
                  }
                  disabled={isSubmitting}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.confirmPassword}
                </Form.Control.Feedback>
              </Col>
            </Form.Group>

            <Form.Group as={Row}>
              <Col sm={{ span: 10, offset: 2 }}>
                <Button type="submit" disabled={isSubmitting}>
                  Create
                </Button>
              </Col>
            </Form.Group>
          </Form>
        )}
      </Formik>
      <ConfirmModal
        show={modalShow}
        onHide={() => {
          setModalShow(false);
          setMyPass('');
        }}
        myPass={myPass}
        setMyPass={setMyPass}
        createStockist={createStockist}
        formData={formData}
      />
    </>
  );
}

StockistsForm.propTypes = {
  createStockist: PropTypes.func.isRequired,
  status: PropTypes.bool.isRequired,
  userRole: PropTypes.string.isRequired,
  level: PropTypes.number.isRequired,
};

export default StockistsForm;
