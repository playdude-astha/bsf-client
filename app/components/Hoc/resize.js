import React from 'react';
// import { getScreenWidth } from 'utils/common';
const getScreenWidth = () => {
  let screen;
  if (window.innerWidth >= 1150) {
    screen = 'big';
  } else if (window.innerWidth <= 1150 && window.innerWidth > 992) {
    screen = 'medium';
  } else if (window.innerWidth <= 992 && window.innerWidth > 767) {
    screen = 'small';
  } else if (window.innerWidth <= 767 && window.innerWidth > 530) {
    screen = 'xSmall';
  } else if (window.innerWidth <= 530) {
    screen = 'xxSmall';
  }
  console.log('get screen width', window.innerWidth, screen);
  return screen;
};

function withWindow(WrappedComponent) {
  return class extends React.Component {
    constructor() {
      super();
      this.state = {
        isMobile: false,
        isSmallMobile: false,
        isBigMobile: false,
        isNotDesktop: false,
        screen: '',
      };
      this.handleResizeListener = this.handleResizeListener.bind(this);
    }

    handleResizeListener() {
      const width = window.innerWidth;
      const { screen } = this.state;
      const windowSize = getScreenWidth();
      if (screen !== windowSize) {
        this.setState({ screen: windowSize });
      }
      let isMobile = false;
      let isTablet = false;
      let isBigMobile = false;
      let isSmallMobile = false;
      let isNotDesktop = false;

      if (width < 1530) isNotDesktop = true;
      if (width < 1200 && width > 600) isBigMobile = true;
      if (width < 992) isMobile = true;
      if (width < 768) isTablet = true;
      if (width < 531) isSmallMobile = true;
      this.setState({
        isMobile,
        isTablet,
        isBigMobile,
        isSmallMobile,
        isNotDesktop,
      });
    }

    componentDidMount() {
      this.handleResizeListener();
      window.addEventListener('resize', this.handleResizeListener);
    }

    componentDidUnMount() {
      window.removeEventListener('resize');
    }

    render() {
      const {
        isMobile,
        screen,
        isTablet,
        isBigMobile,
        isSmallMobile,
        isNotDesktop,
      } = this.state;
      return (
        <WrappedComponent
          isMobile={isMobile}
          screen={screen}
          isTablet={isTablet}
          isBigMobile={isBigMobile}
          isSmallMobile={isSmallMobile}
          isNotDesktop={isNotDesktop}
          {...this.props}
        />
      );
    }
  };
}
export { withWindow };
