export const SRK_365_BASE_URL = 'https://api.skr365.com';
export const ROLES = {
  SUPER_ADMIN: 'SUPER_ADMIN',
  ADMIN: 'ADMIN',
  AGENT: 'AGENT',
};
export const SUPER_STOCKIST_CONFIG = {
  role: ROLES.AGENT,
  level: 4,
};
export const STOCKIST_CONFIG = {
  role: ROLES.AGENT,
  level: 3,
};
export const MASTER_CONFIG = {
  role: ROLES.AGENT,
  level: 2,
};
export const AGENT_CONFIG = {
  role: ROLES.AGENT,
  level: 1,
};

const SUPER_STOCKIST_ROUTE = {
  menuTitle: 'Super Stockists',
  pageTitle: 'Super Stockists',
  path: 'super-stockists',
  config: SUPER_STOCKIST_CONFIG,
};
const STOCKIST_ROUTE = {
  menuTitle: 'Stockists',
  pageTitle: 'Stockists',
  path: 'stockists',
  config: STOCKIST_CONFIG,
};
const MASTER_ROUTE = {
  menuTitle: 'Masters',
  pageTitle: 'Masters',
  path: 'master',
  config: MASTER_CONFIG,
};
const AGENT_ROUTE = {
  menuTitle: 'Agents',
  pageTitle: 'Agents',
  path: 'agents',
  config: AGENT_CONFIG,
};

export const getManageRoutes = (level, role) => {
  const routes = [];
  let levelToCheck = level;

  if (role === ROLES.SUPER_ADMIN) {
    levelToCheck = ROLES.SUPER_ADMIN;
  }
  switch (levelToCheck) {
    case ROLES.SUPER_ADMIN:
      routes.push(
        SUPER_STOCKIST_ROUTE,
        STOCKIST_ROUTE,
        MASTER_ROUTE,
        AGENT_ROUTE,
      );
      break;
    case 4:
      routes.push(STOCKIST_ROUTE, MASTER_ROUTE, AGENT_ROUTE);
      break;
    case 3:
      routes.push(MASTER_ROUTE, AGENT_ROUTE);
      break;
    case 2:
      routes.push(AGENT_ROUTE);
      break;
    default:
      break;
  }

  return routes;
};
