/* eslint-disable */

require('dotenv').config();
const fs = require('fs');
const chalk = require('chalk');
const AWS = require('aws-sdk');
const mime = require('mime-types');
const zlib = require('zlib');
const { gzip, ungzip } = require('node-gzip');
const log = console.log;

let options = {};

if (process.env.NODE_ENV === 'TESTING') {
  options = {
    accessKeyId: process.env.IAM_USER_KEY,
    secretAccessKey: process.env.IAM_USER_SECRET,
    params: {
      Bucket: process.env.S3_TESTING_BUCKET,
    },
  };
} else if (process.env.NODE_ENV === 'PRODUCTION_HATCH_KIDS') {
  //  Todo: add config for production
  options = {
    accessKeyId: process.env.IAM_USER_KEY,
    secretAccessKey: process.env.IAM_USER_SECRET,
    params: {
      Bucket: process.env.S3_PRODUCTION_BUCKET_KIDS,
    },
  };
} else if (process.env.NODE_ENV === 'DEVELOPMENT_HATCH_KIDS') {
  //  Todo: add config for production
  options = {
    accessKeyId: process.env.IAM_USER_KEY,
    secretAccessKey: process.env.IAM_USER_SECRET,
    params: {
      Bucket: process.env.S3_DEVELOPMENT_BUCKET_KIDS,
    },
  };
} else if (process.env.NODE_ENV === 'PRODUCTION') {
  //  Todo: add config for production
  options = {
    accessKeyId: process.env.IAM_USER_KEY,
    secretAccessKey: process.env.IAM_USER_SECRET,
    params: {
      Bucket: process.env.S3_PRODUCTION_BUCKET,
    },
  };
} else if (process.env.NODE_ENV === 'DEVELOPMENT') {
  //  Todo: add config for production
  options = {
    accessKeyId: process.env.IAM_USER_KEY,
    secretAccessKey: process.env.IAM_USER_SECRET,
    params: {
      Bucket: process.env.S3_DEVELOPMENT_BUCKET,
    },
  };
} else {
  log(chalk.red(`Supported build env: ['PRODUCTION', 'TESTING']`));
  process.exit(1);
}

log('options: ', chalk.blue(JSON.stringify(options)));

const s3HatchBucket = new AWS.S3(options);
let success = 0;
let failed = 0;

fs.readdir(`${__dirname}/../../build`, (error, items) => {
  if (error) {
    log(chalk.red('Error reading the build directory: ', error));
    process.exit(1);
  }
  items.forEach(item => {
    //  Todo: upload this file to the s3 async and print the status of the file : DONE
    fs.readFile(`${__dirname}/../../build/${item}`, async (err, data) => {
      if (err) {
        log(chalk.red('Error reading the file : ', item, err));
        return;
      }
      let putOptions = {};
      let path_with_file = `${__dirname}/../../build/${item}`;

      let mime_type = mime.lookup(path_with_file);
      console.log('mime_type: ', mime_type);
      if (!mime_type) {
        mime_type = 'text/plain';
      } else if (mime_type === 'application/javascript') {
        const normalReadstream = fs.createReadStream(path_with_file);
        const string = await streamToString(normalReadstream);
        const gzipContent = await gzip(string);
        putOptions = {
          ...putOptions,
          Body: gzipContent,
          ContentEncoding: 'gzip',
        };
        //  experimnent
        // const gzip = zlib.createGzip();
        // const textReadStream = fs.createReadStream(path_with_file)
        // const gZipReadStream = fs.createReadStream(textReadStream)
      } else {
        putOptions = {
          ...putOptions,
          Body: fs.createReadStream(path_with_file),
        };
      }
      if (item === 'sw.js' || item === 'index.html') {
        putOptions = {
          ...putOptions,
          CacheControl: 'no-cache',
        };
      }
      //  if the file is javascript convert its content into gzip and then upload

      uploadObjectS3({
        ContentType: mime_type,
        Key: item,
        Bucket: `${options.params.Bucket}`,
        ACL: 'public-read',
        ...putOptions,
      })
        .then(res => {
          success++;
          log(chalk.green('Successfully uploaded : ', item));
          log(
            chalk.green(
              `Items Status: Success: ${success} Failed: ${failed} Total: ${
                items.length
              }`,
            ),
          );
        })
        .catch(error => {
          failed++;
          log(chalk.red('Error uploading the file : ', item, error));
        });
    });
  });
});

// if (process.env.NODE_ENV === 'production') {
// } else if (process.env.NODE_ENV === 'development') {
// }

const uploadObjectS3 = params =>
  new Promise((resolve, reject) =>
    s3HatchBucket.putObject(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    }),
  );

function streamToString(stream) {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => chunks.push(chunk));
    stream.on('error', reject);
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  });
}
